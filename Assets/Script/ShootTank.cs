﻿using System.Collections.Generic;
using UnityEngine;

public class ShootTank : MonoBehaviour {

    public List<GameObject> tankInRange;
    private float lastShotTime;
    private TowerData towerData;

    // Use this for initialization
    void Start () {

        tankInRange = new List<GameObject>();
        lastShotTime = Time.time;
        towerData = gameObject.GetComponentInChildren<TowerData>();

    }

    void Update()
    {
        GameObject target = null;
        // 1
        float minimalTankDistance = float.MaxValue;
        foreach (GameObject tank in tankInRange)
        {
            float distanceToGoal = tank.GetComponent<MoveTank>().DistanceToGoal();
            if (distanceToGoal < minimalTankDistance)
            {
                target = tank;
                minimalTankDistance = distanceToGoal;
            }
        }
        // 2
        if (target != null)
        {
            if (Time.time - lastShotTime > towerData.CurrentLevel.fireRate)
            {
                Shoot(target.GetComponent<Collider2D>());
                lastShotTime = Time.time;
            }
            // 3
            Vector3 direction = gameObject.transform.position - target.transform.position;
            gameObject.transform.rotation = Quaternion.AngleAxis(
                Mathf.Atan2(direction.y, direction.x) * 180 / Mathf.PI,
                new Vector3(0, 0, 1));
        }

    }

    void Shoot(Collider2D target)
    {
        GameObject bulletPrefab = towerData.CurrentLevel.bullet;
        // 1 
        Vector3 startPosition = gameObject.transform.position;
        Vector3 targetPosition = target.transform.position;
        startPosition.z = bulletPrefab.transform.position.z;
        targetPosition.z = bulletPrefab.transform.position.z;

        // 2 
        GameObject newBullet = (GameObject)Instantiate(bulletPrefab);
        newBullet.transform.position = startPosition;
        BulletBehavior bulletComp = newBullet.GetComponent<BulletBehavior>();
        bulletComp.target = target.gameObject;
        bulletComp.startPosition = startPosition;
        bulletComp.targetPosition = targetPosition;

    }

    // 1
    void OnTankDestroy(GameObject tank)
    {
        tankInRange.Remove(tank);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        // 2
        if (other.gameObject.tag.Equals("Tank"))
        {
            tankInRange.Add(other.gameObject);
            TankDestructionDelegate del =
                other.gameObject.GetComponent<TankDestructionDelegate>();
            del.tankDelegate += OnTankDestroy;
        }
    }
    // 3
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals("Tank"))
        {
            tankInRange.Remove(other.gameObject);
            TankDestructionDelegate del =
            other.gameObject.GetComponent<TankDestructionDelegate>();
            del.tankDelegate -= OnTankDestroy;
        }
    }

}
