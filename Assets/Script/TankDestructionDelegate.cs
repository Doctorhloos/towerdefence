﻿using UnityEngine;

public class TankDestructionDelegate : MonoBehaviour {

    public delegate void TankDelegate(GameObject tank);
    public TankDelegate tankDelegate;

    void OnDestroy()
    {
        if (tankDelegate != null)
        {
            tankDelegate(gameObject);
        }
    }
}
